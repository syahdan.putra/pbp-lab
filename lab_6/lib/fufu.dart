import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class Fufu extends StatelessWidget {
  final String title;
  const Fufu({Key? key, required this.title}) : super(key: key);
  Widget build(BuildContext context) {
    return MaterialApp(home: Scaffold(
      appBar: AppBar(
        title: Text(title),
        backgroundColor: Colors.teal[400],
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        items: const <BottomNavigationBarItem> [
          BottomNavigationBarItem(
            icon: Icon(Icons.house_siding),
            label: 'Dashboard',
          ),
          BottomNavigationBarItem(
             icon: Icon(Icons.view_carousel),
             label: 'Fufu',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'User',
          ),
        ],
        currentIndex: 1,
        selectedItemColor: Colors.teal[600],
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 30, top: 5.0),
                child: Text(
                  "From us For Us (FUFU)",
                  style: Theme.of(context).textTheme.headline1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 50, top: 5.0),
                child: Text(
                  "A simple message to cheerish each other during pandemic",
                  style: Theme.of(context).textTheme.bodyText1,
                )
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: CarouselSlider(
                    options: CarouselOptions(
                    height: 300,
                    viewportFraction: 0.5,
                    autoPlay: true,
                    autoPlayInterval: const Duration(seconds: 4),
                    autoPlayAnimationDuration: const Duration(milliseconds: 800),
                    autoPlayCurve: Curves.fastOutSlowIn,
                    enlargeCenterPage: true,
                    scrollDirection: Axis.horizontal,),
                    items: [
                      Container (
                        width: 250,
                        margin: const EdgeInsets.all(6.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          border: Border.all(color: Colors.black),
                        ),
                        child : Column (
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding (
                              padding: const EdgeInsets.only(left: 15.0, top: 25.0),
                              child: Text (
                                "From: Michael",
                                style: Theme.of(context).textTheme.headline2,
                              ),
                            ),
                            Padding (
                              padding: const EdgeInsets.all(15.0),
                              child: Text (
                                  "jaga kesehatan ya semuanya!",
                                  style: Theme.of(context).textTheme.bodyText2,
                              )
                            ),
                          ]),
                      ),
                      Container (
                        width: 250,
                        margin: const EdgeInsets.all(6.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          border: Border.all(color: Colors.black),
                        ),
                        child : Column (
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding (
                              padding: const EdgeInsets.only(left: 15.0, top: 25.0),
                              child: Text (
                                "From: Bobby",
                                style: Theme.of(context).textTheme.headline2,
                              ),
                            ),
                            Padding (
                              padding: const EdgeInsets.all(15.0),
                              child: Text (
                                  "Indonesia bisa bangkit dari corona !!!",
                                  style: Theme.of(context).textTheme.bodyText2,
                              )
                            ),
                          ]),
                      ),
                      Container (
                        width: 250,
                        margin: const EdgeInsets.all(6.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          border: Border.all(color: Colors.black),
                        ),
                        child : Column (
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding (
                              padding: const EdgeInsets.only(left: 15.0, top: 25.0),
                              child: Text (
                                "From: Syahdan",
                                style: Theme.of(context).textTheme.headline2,
                              ),
                            ),
                            Padding (
                              padding: const EdgeInsets.all(15.0),
                              child: Text (
                                  "Masker dan vaksin adalah pasangan serasi",
                                  style: Theme.of(context).textTheme.bodyText2,
                              )
                            ),
                          ]),
                      ),
                    ],),
              ),
          ]),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.add),
        backgroundColor: Colors.teal[600],
      ),
      )
    );
  }
}