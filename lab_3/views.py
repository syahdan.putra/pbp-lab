from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from datetime import datetime, date
from lab_1.models import Friend
from .forms import FriendForm

mhs_name = 'Syahdan Putra Adriatama'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002,5,22)
npm = 2006486001

# Create your views here.
@login_required(None, "next", "/admin/login")
def index(request):
    friend = Friend.objects.all()
    response = {'friends':friend}
    return render(request, 'lab3_index.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

@login_required(None, "next", "/admin/login")
def add_friend(request):
    form = FriendForm(request.POST or None)
    response = {"form" : form}
    if (form.is_valid and request.method == "POST"):
        form.save()
        return HttpResponseRedirect("/lab-3")
    return render(request, "lab3_form.html", response)