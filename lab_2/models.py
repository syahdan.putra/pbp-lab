from django.db import models

# Create your models here.
class Note(models.Model):
    To = models.TextField()
    From = models.TextField()
    Title = models.TextField()
    Message = models.TextField()