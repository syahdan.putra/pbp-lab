import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Sign Up",
    home: BelajarForm(),
    theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.teal[600],
        fontFamily: 'Sora',
        textTheme: const TextTheme(
          headline1: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
            color: Colors.black),
        )
      ),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  String value = "";

  @override
  Widget build(BuildContext context) {
    final unameController = TextEditingController();
    final passController = TextEditingController();

    @override
    void dispose() {
      unameController.dispose();
      passController.dispose();
      super.dispose();
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Sign Up ZonaHijau"),
        backgroundColor: Colors.teal[600],
      ),
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width/2,
          child: Card(
            child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "SIGN UP AN ACCOUNT",
                        style: Theme.of(context).textTheme.headline1,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        maxLength: 10,
                        decoration: new InputDecoration(
                          hintText: "Sam Smith",
                          labelText: "Username ...",
                          icon: Icon(Icons.people),
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Username can\'t be empty';
                          }
                          return null;
                        },
                        controller: unameController,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        obscureText: true,
                        decoration: new InputDecoration(
                          labelText: "Enter password",
                          icon: Icon(Icons.vpn_key_rounded),
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Password can\'t be empty';
                          }
                          return null;
                        },
                         controller: passController,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        obscureText: true,
                        decoration: new InputDecoration(
                          labelText: "Re-enter Password...",
                          icon: Icon(Icons.vpn_key_rounded),
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Enter password confirmation';
                          }
                          if (value != passController.text) {
                            return 'Password missmatch';
                          }
                          return null;
                        },
                      ),
                    ),
                    ElevatedButton(
                      child: Text(
                        "Sign Up",
                        style: TextStyle(color: Colors.white),
                      ),
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(Colors.teal[600]),
                        ),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          print("Sign in berhasil. Hi, " + unameController.text + "!");
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        )
      ),
    );
  }
}